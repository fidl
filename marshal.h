/*
 * QEMU Marshalling Framework
 *
 * Copyright IBM, Corp. 2010
 *
 * Authors:
 *  Anthony Liguori <aliguori@us.ibm.com>
 *
 * This work is licensed under the terms of the GNU GPL, version 2.  See
 * the COPYING file in the top-level directory.
 */

#ifndef QEMU_MARSHAL_H
#define QEMU_MARSHAL_H

#include <stdint.h>
#include <stdbool.h>

#include "error.h"

typedef struct MarshallerOps MarshallerOps;
typedef struct Marshaller Marshaller;

struct MarshallerOps
{
    void (*m_uint8)(Marshaller *m, uint8_t *v, const char *n, Error **errp);
    void (*m_uint16)(Marshaller *m, uint16_t *v, const char *n, Error **errp);
    void (*m_uint32)(Marshaller *m, uint32_t *v, const char *n, Error **errp);
    void (*m_uint64)(Marshaller *m, uint64_t *v, const char *n, Error **errp);

    void (*m_int8)(Marshaller *m, int8_t *v, const char *n, Error **errp);
    void (*m_int16)(Marshaller *m, int16_t *v, const char *n, Error **errp);
    void (*m_int32)(Marshaller *m, int32_t *v, const char *n, Error **errp);
    void (*m_int64)(Marshaller *m, int64_t *v, const char *n, Error **errp);

    void (*m_double)(Marshaller *m, double *v, const char *n, Error **errp);
    void (*m_bool)(Marshaller *m, bool *v, const char *n, Error **errp);

    void (*start_struct)(Marshaller *m, const char *t, const char *n, Error **errp);
    void (*end_struct)(Marshaller *m, Error **errp);

    void (*start_array)(Marshaller *m, const char *n, Error **errp);
    void (*end_array)(Marshaller *m, Error **errp);

    void (*start_feature)(Marshaller *m, bool *v, const char *n, Error **errp);
    void (*end_feature)(Marshaller *m, Error **errp);
};

struct Marshaller
{
    MarshallerOps *ops;
};

static inline void marshal_uint8(Marshaller *m, uint8_t *v, const char *n, Error **errp)
{
    m->ops->m_uint8(m, v, n, errp);
}

static inline void marshal_uint16(Marshaller *m, uint16_t *v, const char *n, Error **errp)
{
    m->ops->m_uint16(m, v, n, errp);
}

static inline void marshal_uint32(Marshaller *m, uint32_t *v, const char *n, Error **errp)
{
    m->ops->m_uint32(m, v, n, errp);
}

static inline void marshal_uint64(Marshaller *m, uint64_t *v, const char *n, Error **errp)
{
    m->ops->m_uint64(m, v, n, errp);
}

static inline void marshal_int8(Marshaller *m, int8_t *v, const char *n, Error **errp)
{
    m->ops->m_int8(m, v, n, errp);
}

static inline void marshal_int16(Marshaller *m, int16_t *v, const char *n, Error **errp)
{
    m->ops->m_int16(m, v, n, errp);
}

static inline void marshal_int32(Marshaller *m, int32_t *v, const char *n, Error **errp)
{
    m->ops->m_int32(m, v, n, errp);
}

static inline void marshal_int64(Marshaller *m, int64_t *v, const char *n, Error **errp)
{
    m->ops->m_int64(m, v, n, errp);
}

static inline void marshal_double(Marshaller *m, double *v, const char *n, Error **errp)
{
    m->ops->m_double(m, v, n, errp);
}

static inline void marshal_bool(Marshaller *m, bool *v, const char *n, Error **errp)
{
    m->ops->m_bool(m, v, n, errp);
}

static inline void marshal_start_struct(Marshaller *m, const char *t, const char *n, Error **errp)
{
    m->ops->start_struct(m, t, n, errp);
}

static inline void marshal_end_struct(Marshaller *m, Error **errp)
{
    m->ops->end_struct(m, errp);
}

static inline void marshal_start_array(Marshaller *m, const char *n, Error **errp)
{
    m->ops->start_array(m, n, errp);
}

static inline void marshal_end_array(Marshaller *m, Error **errp)
{
    m->ops->end_array(m, errp);
}

static inline void marshal_start_feature(Marshaller *m, bool *v, const char *n, Error **errp)
{
    m->ops->start_feature(m, v, n, errp);
}

static inline void marshal_end_feature(Marshaller *m, Error **errp)
{
    m->ops->end_feature(m, errp);
}

static inline void marshal_int(Marshaller *m, int *v, const char *n, Error **errp)
{
    int value = *v;
    marshal_int32(m, &value, n, errp);
    if (!error_is_set(errp)) {
        *v = value;
    }
}

#endif
