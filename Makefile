CC=gcc
CFLAGS = -g -Wall -O

CFLAGS += $(shell pkg-config --cflags libxml-2.0)
LDLIBS += $(shell pkg-config --libs libxml-2.0)

all: testAll xml-reader.o

testAll: xml-writer.o error.o test-marshal.o main.o xml-reader.o
	$(CC) -o testAll $^ $(LDLIBS)

xml-writer.o: xml-writer.c error.h marshal.h xml-writer.h

xml-reader.o: xml-reader.c error.h marshal.h xml-reader.h

error.o: error.c error.h

test-marshal.o: test-marshal.h test.h marshal.h test-marshal.c

%-marshal.c: %.h
	./qemu-gen-marshal $^ $@

%-marshal.h: %.h
	./qemu-gen-marshal --header $^ $@

main.o: main.c xml-writer.h test-marshal.h

clean:
	$(RM) *.o *~ *-marshal.[ch] test.xml testAll *.pyc
