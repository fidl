/*
 * QEMU Marshalling Framework
 * Test driver
 *
 * Copyright IBM, Corp. 2010
 *
 * Authors:
 *  Anthony Liguori <aliguori@us.ibm.com>
 *
 * This work is licensed under the terms of the GNU GPL, version 2.  See
 * the COPYING file in the top-level directory.
 */

#include <string.h>

#include "test-marshal.h"
#include "xml-writer.h"
#include "xml-reader.h"

int main(int argc, char **argv)
{
    TestDevice test = {};
    Marshaller *m;
    FILE *f;
    Error *err = NULL;
    int i;

    test.mem_len = 32;
    test.mem = malloc(test.mem_len);
    for (i = 0; i < test.mem_len; i++) {
        test.mem[i] = i * 3;
    }

    test.bank2.bank2_enabled = true;
    test.bank2.mem_len = 0;
    test.bank2.mem = NULL;

    test.bank2.bank3.bank3_enabled = true;
    memset(test.bank2.bank3.mem, 0, sizeof(test.bank2.bank3.mem));

    f = fopen("test.xml", "w");
    m = xml_writer_open(f);
    marshal_TestDevice(m, &test, "test", NULL);
    xml_writer_close(m);
    fclose(f);

    memset(test.mem, 0, test.mem_len);

    f = fopen("test.xml", "r");
    m = xml_reader_open(f);
    marshal_TestDevice(m, &test, "test", &err);
    xml_reader_close(m);
    fclose(f);

    if (err) {
        fprintf(stderr, "FAIL: %s\n", err->message);
        error_free(err);
        return 1;
    }

    m = xml_writer_open(stdout);
    marshal_TestDevice(m, &test, "test", NULL);
    xml_writer_close(m);

    free(test.mem);

    return 0;
}
