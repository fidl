/*
 * QEMU Marshalling Framework
 * XML marshaller
 *
 * Copyright IBM, Corp. 2010
 *
 * Authors:
 *  Anthony Liguori <aliguori@us.ibm.com>
 *
 * This work is licensed under the terms of the GNU GPL, version 2.  See
 * the COPYING file in the top-level directory.
 */

#include <string.h>
#include <inttypes.h>
#include <malloc.h>

#include "xml-writer.h"

#define container_of(obj, type, member) \
    ((type *)(((char *)obj) - offsetof(type, member)))

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

#define INDENT_WIDTH 4
#define INDENT_CHAR ' '
#define EOL "\n"
#define MAX_FEATURES 100

typedef struct XMLWriter
{
    Marshaller m;
    FILE *filp;
    int indent;
    int feature;
    bool features[MAX_FEATURES];
} XMLWriter;

static XMLWriter *to_xm(Marshaller *m)
{
    return container_of(m, XMLWriter, m);
}

static const char *indent(size_t count)
{
    static char buffer[1024];

    memset(buffer, INDENT_CHAR, sizeof(buffer));
    count = MIN(count * INDENT_WIDTH, sizeof(buffer) - 1);
    buffer[count] = 0;

    return buffer;
}

static const char *expand_name(const char *n)
{
    if (n) {
        static char buffer[4096];
        snprintf(buffer, sizeof(buffer), " name=\"%s\"", n);
        return buffer;
    }
    return "";
}

static const char *expand_type(const char *t)
{
    if (t) {
        static char buffer[4096];
        snprintf(buffer, sizeof(buffer), " type=\"%s\"", t);
        return buffer;
    }
    return "";
}

static void emit(XMLWriter *xm, const char *fmt, ...)
{
    va_list ap;

    if (!xm->features[xm->feature - 1]) {
        return;
    }

    fprintf(xm->filp, "%s", indent(xm->indent));
    va_start(ap, fmt);
    vfprintf(xm->filp, fmt, ap);
    va_end(ap);
    fprintf(xm->filp, "%s", EOL);
}

static void xml_m_uint8(Marshaller *m, uint8_t *v, const char *n, Error **errp)
{
    XMLWriter *xm = to_xm(m);

    emit(xm, "<uint8%s>%u</uint8>", expand_name(n), *v);
}

static void xml_m_uint16(Marshaller *m, uint16_t *v, const char *n, Error **errp)
{
    XMLWriter *xm = to_xm(m);
    emit(xm, "<uint16%s>%u</uint16>", expand_name(n), *v);
}

static void xml_m_uint32(Marshaller *m, uint32_t *v, const char *n, Error **errp)
{
    XMLWriter *xm = to_xm(m);
    emit(xm, "<uint32%s>%u</uint32>", expand_name(n), *v);
}

static void xml_m_uint64(Marshaller *m, uint64_t *v, const char *n, Error **errp)
{
    XMLWriter *xm = to_xm(m);
    emit(xm, "<uint64%s>%" PRIu64 "</uint64>", expand_name(n), *v);
}

static void xml_m_int8(Marshaller *m, int8_t *v, const char *n, Error **errp)
{
    XMLWriter *xm = to_xm(m);
    emit(xm, "<int8%s>%u</int8>", expand_name(n), *v);
}

static void xml_m_int16(Marshaller *m, int16_t *v, const char *n, Error **errp)
{
    XMLWriter *xm = to_xm(m);
    emit(xm, "<int16%s>%u</int16>", expand_name(n), *v);
}

static void xml_m_int32(Marshaller *m, int32_t *v, const char *n, Error **errp)
{
    XMLWriter *xm = to_xm(m);
    emit(xm, "<int32%s>%u</int32>", expand_name(n), *v);
}

static void xml_m_int64(Marshaller *m, int64_t *v, const char *n, Error **errp)
{
    XMLWriter *xm = to_xm(m);
    emit(xm, "<int64%s>%" PRId64 "</int64>", expand_name(n), *v);
}

static void xml_m_double(Marshaller *m, double *v, const char *n, Error **errp)
{
    XMLWriter *xm = to_xm(m);
    emit(xm, "<double%s>%f</double>", expand_name(n), *v);
}

static void xml_m_bool(Marshaller *m, bool *v, const char *n, Error **errp)
{
    XMLWriter *xm = to_xm(m);
    emit(xm, "<bool%s>%s</bool>", expand_name(n), *v ? "true" : "false");
}

static void xml_start_struct(Marshaller *m, const char *t, const char *n, Error **errp)
{
    XMLWriter *xm = to_xm(m);
    emit(xm, "<struct%s%s>", expand_name(n), expand_type(t));
    xm->indent++;
}

static void xml_end_struct(Marshaller *m, Error **errp)
{
    XMLWriter *xm = to_xm(m);
    xm->indent--;
    emit(xm, "</struct>");
}

static void xml_start_array(Marshaller *m, const char *n, Error **errp)
{
    XMLWriter *xm = to_xm(m);
    emit(xm, "<array%s>", expand_name(n));
    xm->indent++;
}

static void xml_end_array(Marshaller *m, Error **errp)
{
    XMLWriter *xm = to_xm(m);
    xm->indent--;
    emit(xm, "</array>");
}

static void xml_start_feature(Marshaller *m, bool *v, const char *n, Error **errp)
{
    XMLWriter *xm = to_xm(m);
    bool previous_feature;

    previous_feature = xm->features[xm->feature - 1];
    xm->features[xm->feature++] = previous_feature && *v;

    emit(xm, "<feature%s>", expand_name(n));
    xm->indent++;
}

static void xml_end_feature(Marshaller *m, Error **errp)
{
    XMLWriter *xm = to_xm(m);

    xm->indent--;
    emit(xm, "</feature>");
    xm->feature--;
}

static MarshallerOps xml_writer_ops = {
    .m_uint8 = xml_m_uint8,
    .m_uint16 = xml_m_uint16,
    .m_uint32 = xml_m_uint32,
    .m_uint64 = xml_m_uint64,
    .m_int8 = xml_m_int8,
    .m_int16 = xml_m_int16,
    .m_int32 = xml_m_int32,
    .m_int64 = xml_m_int64,
    .m_double = xml_m_double,
    .m_bool = xml_m_bool,
    .start_struct = xml_start_struct,
    .end_struct = xml_end_struct,
    .start_array = xml_start_array,
    .end_array = xml_end_array,
    .start_feature = xml_start_feature,
    .end_feature = xml_end_feature,
};

Marshaller *xml_writer_open(FILE *filp)
{
    XMLWriter *xm;

    xm = malloc(sizeof(*xm));
    memset(xm, 0, sizeof(*xm));
    xm->m.ops = &xml_writer_ops;
    xm->filp = filp;

    xm->features[xm->feature++] = true;

    emit(xm, "<?xml version=\"1.0\"?>");
    emit(xm, "<device>");
    xm->indent++;
    return &xm->m;
}

void xml_writer_close(Marshaller *m)
{
    XMLWriter *xm = to_xm(m);
    xm->indent--;
    emit(xm, "</device>");
    fflush(xm->filp);
    free(xm);
}
