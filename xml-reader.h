/*
 * QEMU Marshalling Framework
 * XML unmarshaller
 *
 * Copyright IBM, Corp. 2010
 *
 * Authors:
 *  Anthony Liguori <aliguori@us.ibm.com>
 *
 * This work is licensed under the terms of the GNU GPL, version 2.  See
 * the COPYING file in the top-level directory.
 */

#ifndef XML_READER_H
#define XML_READER_H

#include "marshal.h"

Marshaller *xml_reader_open(FILE *filep);
void xml_reader_close(Marshaller *m);

#endif
