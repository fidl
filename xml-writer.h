/*
 * QEMU Marshalling Framework
 * XML marshaller
 *
 * Copyright IBM, Corp. 2010
 *
 * Authors:
 *  Anthony Liguori <aliguori@us.ibm.com>
 *
 * This work is licensed under the terms of the GNU GPL, version 2.  See
 * the COPYING file in the top-level directory.
 */

#ifndef XML_WRITER_H
#define XML_WRITER_H

#include <stdio.h>
#include "marshal.h"

Marshaller *xml_writer_open(FILE *filp);
void xml_writer_close(Marshaller *m);

#endif

