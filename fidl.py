##
# Flexible IDL framework
#
# Copyright IBM, Corp. 2010
#
# Authors:
#  Anthony Liguori <aliguori@us.ibm.com>
#
# This work is licensed under the terms of the GNU GPL, version 2.  See
# the COPYING file in the top-level directory.
##

from spark import GenericScanner, GenericParser

# Lexical token representation.  It really just lets us associate a tag with
# the token value for purposes of classification.
class Token(object):
    def __init__(self, kind, value=None):
        self.kind = kind
        if value == None:
            self.value = kind
        else:
            self.value = value

    def __cmp__(self, rhs):
        return cmp(self.kind, rhs)

    def __repr__(self):
        return '[%s %s]' % (self.kind, self.value)

# Lexical scanner.  It skips whitespace and C99 and C89 comments.  It also
# skips cpp directives which is somewhat unusual.

class IdlScanner(GenericScanner):
    keywords = ['const', 'struct', 'typedef', 'void',
                '__capacity_is__', '__feature__']

    def __init__(self):
        GenericScanner.__init__(self)

    def tokenize(self, input):
        self.rv = []
        GenericScanner.tokenize(self, input)
        return self.rv
    
    def t_whitespace(self, s):
        r' \s+ '
        pass

    def t_c89_comment(self, s):
        r' /\*([^\*]|\n|\*[^\/])*\*/ '
        pass

    def t_c99_comment(self, s):
        r' //.*?\n '
        pass

    def t_cpp_directive(self, s):
        r' \#(.*?\\\n)*.*?\n '
        pass
        
    def t_symbol(self, s):
        r' [A-Za-z_][A-Za-z0-9_]* '
        if s in self.keywords:
            self.rv.append(Token(s))
        else:
            self.rv.append(Token('symbol', s))

    def t_integer(self, s):
        r' \-?[1-9][0-9]+|0([Xx][0-9a-fA-F]+)? '
        self.rv.append(Token('integer', s))

    def t_op(self, s):
        r' [*,\(\);{}\[\]] '
        self.rv.append(s)

def scan(f):
    input = f.read()
    scanner = IdlScanner()
    return scanner.tokenize(input)

# Abstract Syntax Tree node.  
class AST(object):
    def __init__(self, kind, **kwds):
        self.kind = kind
        self.kwds = kwds

    def __getattr__(self, key):
        if self.kwds.has_key(key):
            return self.kwds[key]
        return object.__getattr__(self, key)

    def __repr__(self):
        return '(%s, %s)' % (self.kind, self.kwds)

# Grammar implementation.  This is only a subset of C.  It does not support
# enums or unions.  It also only supports single levels of pointers and only
# support const at the beginning of a type.  It does not support static or the
# extern keywords.
#
# It's meant to support the type of C you'd find in a typical header file.  It
# assumes that a CPP pass has not been performed.
class IdlParser(GenericParser):
    def __init__(self, start='statements'):
        GenericParser.__init__(self, start)

    def p_primitive_1(self, args):
        '''
          primitive ::= symbol
        '''
        return AST('type', value=args[0].value)

    def p_primitive_2(self, args):
        ' primitive ::= structure '
        return args[0]

    def p_structure(self, args):
        '''
          structure ::= struct symbol
          structure ::= struct symbol { members }
          structure ::= struct { members }
          structure ::= struct { members } __feature__ ( symbol )
        '''
        if len(args) == 2:
            return AST('struct', name=args[1].value)
        elif len(args) == 4:
            return AST('struct', name=None, members=args[2])
        elif len(args) == 5:
            return AST('struct', name=args[1].value, members=args[3])
        else:
            return AST('struct', name=None, members=args[2],
                       feature=args[6].value)

    def p_type_1(self, args):
        ' type ::= const primitive * '
        return AST('input', base=args[1])

    def p_type_2(self, args):
        ' type ::= primitive * '
        return AST('output', base=args[0])

    def p_type_3(self, args):
        ' type ::= primitive * * '
        return AST('output', base=AST('array', base=args[0]))

    def p_type_4(self, args):
        ' type ::= primitive '
        return args[0]

    def p_type_5(self, args):
        ' type ::= primitive * __capacity_is__ ( symbol ) '
        return AST('varray',
                   base=args[0],
                   size=args[4].value)

    def p_arg_1(self, args):
        ' arg ::= type symbol'
        return AST('arg', type=args[0], name=args[1].value)

    def p_arg_2(self, args):
        ' arg ::= type symbol [ symbol ] '
        return AST('arg',
                   type=AST('array', base=args[0]),
                   name=args[1].value, 
                   size=args[3].value)

    def p_arg_3(self, args):
        ' arg ::= type symbol [ integer ] '
        return AST('arg',
                   type=AST('array', base=args[0]),
                   name=args[1].value, 
                   size=args[3].value)

    def p_arglist_1(self, args):
        ' arglist ::= arg '
        return [args[0]]

    def p_arglist_2(self, args):
        ' arglist ::= arglist , arg '
        return args[0] + [args[2]]

    def p_argslist_1(self, args):
        ' argslist ::= arglist '
        return args[0]

    def p_argslist_2(self, args):
        ' argslist ::= void '
        return []

    def p_definition_1(self, args):
        ' definition ::= void symbol ( argslist ) '
        return AST('def', name=args[1].value, params=args[3])

    def p_definition_2(self, args):
        ' definition ::= type symbol ( argslist ) '
        return AST('def', name=args[1].value, retval=args[0], params=args[3])

    def p_members_1(self, args):
        ' members ::= arg ; '
        return [args[0]]

    def p_members_2(self, args):
        ' members ::= members arg ; '
        return args[0] + [args[1]]

    def p_type_definition(self, args):
        ' type_definition ::= typedef type symbol '
        return AST('typedef', alias=args[2].value, base=args[1])

    def p_statement_1(self, args):
        '''
          statement ::= type_definition ;
          statement ::= definition ;
        '''
        return [args[0]]

    def p_statement_2(self, args):
        ' statement ::= structure ; '
        return [AST('struct_decl', struct=args[0])]

    def p_statements_1(self, args):
        ' statements ::= statement '
        return args[0]

    def p_statements_2(self, args):
        ' statements ::= statements statement '
        return args[0] + args[1]

def parse(tokens):
    parser = IdlParser()
    return parser.parse(tokens)

# Walk the AST tree.  Each AST node type has a method that only knows enough
# to walk its children.  This class should be subclassed to transform the
# tree.
class ASTWalker(object):
    def __init__(self):
        pass

    def default(self, node):
        kwds = {}
        for key in node.kwds:
            value = getattr(node, key)
            if type(value) in [AST, list]:
                kwds[key] = self.walk(value)
            else:
                kwds[key] = value
        return AST(node.kind, **kwds)

    def walk_node(self, node):
        name = 'n_%s' % node.kind
        if hasattr(self, name):
            fn = getattr(self, name)
            node = fn(node)
        else:
            node = self.default(node)
        return node

    def walk(self, tree):
        if type(tree) == AST:
            return self.walk_node(tree) 
        else:
            return filter(lambda x: x != None, map(self.walk_node, tree))

# Pretty print an AST
class PrettyWalker(ASTWalker):
    def __init__(self):
        ASTWalker.__init__(self)

    def print_indent(self, indent):
        for i in range(indent):
            print ' ',

    def default(self, nodes, indent=0):
        if type(nodes) != list:
            nodes = [nodes]

        for node in nodes:
            self.print_indent(indent)
            print '%s:' % node.kind
            for key in node.kwds:
                value = getattr(node, key)
                if type(value) == AST or type(value) == list:
                    self.default(value, indent + 1)
                else:
                    self.print_indent(indent + 1)
                    print '%s = %s' % (key, value)
