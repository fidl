/*
 * QEMU Marshalling Framework
 * XML unmarshaller
 *
 * Copyright IBM, Corp. 2010
 *
 * Authors:
 *  Anthony Liguori <aliguori@us.ibm.com>
 *
 * This work is licensed under the terms of the GNU GPL, version 2.  See
 * the COPYING file in the top-level directory.
 */

#include <string.h>
#include <inttypes.h>
#include <malloc.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#include "xml-reader.h"

#define container_of(obj, type, member) \
    ((type *)(((char *)obj) - offsetof(type, member)))

#define MAX_FEATURES 100

typedef struct XMLReader
{
    Marshaller m;
    xmlDoc *doc;

    /* we use these three values to transverse the XML tree.  last_node points
     * to the last node we visited.  We really use this as a backlink to the
     * parent in the event node == NULL.
     *
     * There's a special case though where the children of a node is NULL.  We
     * don't have a node that we can point to who's parent is us so we use
     * no_kids as a hack.  We can probably simplify by just storing the parent.
     */
    xmlNode *last_node;
    xmlNode *node;
    int no_kids;

    int feature;
    bool features[MAX_FEATURES];
} XMLReader;

#define CSTR(a) ((const char*)(a))

static XMLReader *to_xm(Marshaller *m)
{
    return container_of(m, XMLReader, m);
}

static bool strequals(const char *lhs, const char *rhs)
{
    return !!(strcmp(lhs, rhs) == 0);
}

static xmlNode *next_node(xmlNode *n)
{
    do {
        n = n->next;
    } while (n && n->type != XML_ELEMENT_NODE);
    return n;
}

static xmlNode *first_node(xmlNode *n)
{
    while (n && n->type != XML_ELEMENT_NODE) {
        n = n->next;
    }
    return n;
}

static bool check_disabled(Marshaller *m)
{
    XMLReader *xm = to_xm(m);
    return !xm->features[xm->feature - 1];
}

static void xml_m_uint(Marshaller *m, uint64_t *v, const char *t, const char *n, Error **errp)
{
    XMLReader *xm = to_xm(m);
    xmlChar *content;
    int ret;

    if (xm->node == NULL) {
        error_set(errp, "xml-reader", 0,
                  "Unexpected end of section looking for `%s' node `%s' on line %d",
                  t, n, xm->last_node->line);
        return;
    }
    if (!strequals(CSTR(xm->node->name), t)) {
        error_set(errp, "xml-reader", 0,
                  "Expecting node type `%s', got `%s' while parsing `%s' on line %d",
                  t, CSTR(xm->node->name), n, xm->node->line);
        return;
    }
    content = xmlNodeGetContent(xm->node);
    if (content) {
        ret = sscanf(CSTR(content), "%" PRIu64, v);
        xmlFree(content);
    }
    if (!content || ret != 1) {
        error_set(errp, "xml-reader", 0,
                  "Failed to parse type `%s' node `%s' on line %d",
                  t, n, xm->node->line);
        return;
    }

    xm->last_node = xm->node;
    xm->node = next_node(xm->node);
}

static void xml_m_int(Marshaller *m, int64_t *v, const char *t, const char *n, Error **errp)
{
    XMLReader *xm = to_xm(m);
    xmlChar *content;
    int ret;

    if (xm->node == NULL) {
        error_set(errp, "xml-reader", 0,
                  "Unexpected end of section looking for `%s' node `%s' on line %d",
                  t, n, xm->last_node->line);
        return;
    }
    if (!strequals(CSTR(xm->node->name), t)) {
        error_set(errp, "xml-reader", 0,
                  "Expecting node type `%s', got `%s' while parsing `%s' on line %d",
                  t, CSTR(xm->node->name), n, xm->node->line);
        return;
    }
    content = xmlNodeGetContent(xm->node);
    if (content) {
        ret = sscanf(CSTR(content), "%" PRId64, v);
        xmlFree(content);
    }
    if (!content || ret != 1) {
        error_set(errp, "xml-reader", 0,
                  "Failed to parse type `%s' node `%s' on line %d",
                  t, n, xm->node->line);
        return;
    }

    xm->last_node = xm->node;
    xm->node = next_node(xm->node);
}

static void xml_m_uint8(Marshaller *m, uint8_t *v, const char *n, Error **errp)
{
    uint64_t value;

    if (check_disabled(m)) {
        return;
    }

    xml_m_uint(m, &value, "uint8", n, errp);
    if (!error_is_set(errp)) {
        *v = value;
    }
}

static void xml_m_uint16(Marshaller *m, uint16_t *v, const char *n, Error **errp)
{
    uint64_t value;

    if (check_disabled(m)) {
        return;
    }

    xml_m_uint(m, &value, "uint16", n, errp);
    if (!error_is_set(errp)) {
        *v = value;
    }
}

static void xml_m_uint32(Marshaller *m, uint32_t *v, const char *n, Error **errp)
{
    uint64_t value;

    if (check_disabled(m)) {
        return;
    }

    xml_m_uint(m, &value, "uint32", n, errp);
    if (!error_is_set(errp)) {
        *v = value;
    }
}

static void xml_m_uint64(Marshaller *m, uint64_t *v, const char *n, Error **errp)
{
    if (check_disabled(m)) {
        return;
    }

    xml_m_uint(m, v, "uint64", n, errp);
}

static void xml_m_int8(Marshaller *m, int8_t *v, const char *n, Error **errp)
{
    int64_t value;

    if (check_disabled(m)) {
        return;
    }

    xml_m_int(m, &value, "int8", n, errp);
    if (!error_is_set(errp)) {
        *v = value;
    }
}

static void xml_m_int16(Marshaller *m, int16_t *v, const char *n, Error **errp)
{
    int64_t value;

    if (check_disabled(m)) {
        return;
    }

    xml_m_int(m, &value, "int16", n, errp);
    if (!error_is_set(errp)) {
        *v = value;
    }
}

static void xml_m_int32(Marshaller *m, int32_t *v, const char *n, Error **errp)
{
    int64_t value;

    if (check_disabled(m)) {
        return;
    }

    xml_m_int(m, &value, "int32", n, errp);
    if (!error_is_set(errp)) {
        *v = value;
    }
}

static void xml_m_int64(Marshaller *m, int64_t *v, const char *n, Error **errp)
{
    if (check_disabled(m)) {
        return;
    }

    xml_m_int(m, v, "int64", n, errp);
}

static void xml_m_double(Marshaller *m, double *v, const char *n, Error **errp)
{
    XMLReader *xm = to_xm(m);
    xmlChar *content;
    int ret;

    if (check_disabled(m)) {
        return;
    }

    if (xm->node == NULL) {
        error_set(errp, "xml-reader", 0,
                  "Unexpected end of section looking for `%s' node `%s' on line %d",
                  "double", n, xm->last_node->line);
        return;
    }
    if (!strequals(CSTR(xm->node->name), "double")) {
        error_set(errp, "xml-reader", 0,
                  "Expecting node type `%s', got `%s' while parsing `%s' on line %d",
                  "double", CSTR(xm->node->name), n, xm->node->line);
        return;
    }
    content = xmlNodeGetContent(xm->node);
    if (content) {
        ret = sscanf(CSTR(content), "%lf", v);
        xmlFree(content);
    }
    if (!content || ret != 1) {
        error_set(errp, "xml-reader", 0,
                  "Failed to parse type `%s' node `%s' on line %d",
                  "double", n, xm->node->line);
        return;
    }

    xm->last_node = xm->node;
    xm->node = next_node(xm->node);
}

static void xml_m_bool(Marshaller *m, bool *v, const char *n, Error **errp)
{
    XMLReader *xm = to_xm(m);
    xmlChar *content;

    if (check_disabled(m)) {
        return;
    }

    if (xm->node == NULL) {
        error_set(errp, "xml-reader", 0,
                  "Unexpected end of section looking for `%s' node `%s' on line %d",
                  "double", n, xm->last_node->line);
        return;
    }
    if (!strequals(CSTR(xm->node->name), "bool")) {
        error_set(errp, "xml-reader", 0,
                  "Expecting node type `%s', got `%s' while parsing `%s' on line %d",
                  "bool", CSTR(xm->node->name), n, xm->node->line);
        return;
    }
    content = xmlNodeGetContent(xm->node);
    if (content) {
        if (strequals(CSTR(content), "true")) {
            *v = true;
        } else if (strequals(CSTR(content), "false")) {
            *v = false;
        } else {
            error_set(errp, "xml-reader", 0,
                      "Invalid value for node type `%s', got `%s', expecting `true' or 'false' on line %d",
                      "bool", content, xm->node->line);
            xmlFree(content);
            return;
        }
        xmlFree(content);
    }
    if (!content) {
        error_set(errp, "xml-reader", 0,
                  "Failed to parse type `%s' node `%s' on line %d",
                  "bool", n, xm->node->line);
        return;
    }

    xm->last_node = xm->node;
    xm->node = next_node(xm->node);
}

static void confirm_prop(xmlNode *node, const char *t, const char *n, const char *p, Error **errp)
{
    xmlChar *v;

    if (t) {
        v = xmlGetProp(node, (xmlChar *)p);
        if (!v) {
            error_set(errp, "xml-reader", 0,
                      "Missing attribute `%s' in `%s' node `%s' on line %d",
                      p, CSTR(node->name), n, node->line);
            return;
        }
        if (!strequals(CSTR(v), t)) {
            error_set(errp, "xml-reader", 0,
                      "Expecting attribute `%s' value of `%s' for `%s' node `%s', got `%s' on line %d",
                      p, t, CSTR(node->name), n, CSTR(v), node->line);
            xmlFree(v);
            return;
        }
        xmlFree(v);
    }
}

static void xml_start_struct(Marshaller *m, const char *t, const char *n, Error **errp)
{
    XMLReader *xm = to_xm(m);

    if (check_disabled(m)) {
        return;
    }

    if (!strequals(CSTR(xm->node->name), "struct")) {
        error_set(errp, "xml-reader", 0, "Expecting node `%s', got `%s' on line %d",
                  CSTR(xm->node->name), n, xm->node->line);
        return;
    }

    confirm_prop(xm->node, t, n, "type", errp);
    if (error_is_set(errp)) {
        return;
    }

    confirm_prop(xm->node, n, n, "name", errp);
    if (error_is_set(errp)) {
        return;
    }
            
    xm->last_node = xm->node;
    xm->node = first_node(xm->node->children);
    xm->no_kids = !!(xm->node == NULL);
}

static void xml_end_struct(Marshaller *m, Error **errp)
{
    XMLReader *xm = to_xm(m);

    if (check_disabled(m)) {
        return;
    }

    if (xm->node != NULL) {
        error_set(errp, "xml-reader", 0, "Extra nodes at end of `%s' on line %d",
                  "struct", xm->node->line);
        return;
    }

    if (xm->no_kids) {
        xm->no_kids = 0;
    } else {
        xm->last_node = xm->last_node->parent;
    }
    xm->node = next_node(xm->last_node);
}

static void xml_start_array(Marshaller *m, const char *n, Error **errp)
{
    XMLReader *xm = to_xm(m);

    if (check_disabled(m)) {
        return;
    }

    if (!strequals(CSTR(xm->node->name), "array")) {
        error_set(errp, "xml-reader", 0, "Expecting node `%s', got `%s' on line %d",
                  CSTR(xm->node->name), n, xm->node->line);
        return;
    }

    confirm_prop(xm->node, n, n, "name", errp);
    if (error_is_set(errp)) {
        return;
    }
            
    xm->last_node = xm->node;
    xm->node = first_node(xm->node->children);
    xm->no_kids = !!(xm->node == NULL);
}

static void xml_end_array(Marshaller *m, Error **errp)
{
    XMLReader *xm = to_xm(m);

    if (check_disabled(m)) {
        return;
    }

    if (xm->node != NULL) {
        error_set(errp, "xml-reader", 0, "Extra nodes at end of `%s' on line %d",
                  "array", xm->node->line);
        return;
    }

    if (xm->no_kids) {
        xm->no_kids = 0;
    } else {
        xm->last_node = xm->last_node->parent;
    }
    xm->node = next_node(xm->last_node);
}

static void xml_start_feature(Marshaller *m, bool *v, const char *n, Error **errp)
{
    XMLReader *xm = to_xm(m);
    bool previous_feature;

    previous_feature = xm->features[xm->feature - 1];
    xm->features[xm->feature++] = previous_feature && *v;

    if (check_disabled(m)) {
        return;
    }

    if (!strequals(CSTR(xm->node->name), "feature")) {
        error_set(errp, "xml-reader", 0, "Expecting node `%s', got `%s' on line %d",
                  "feature", CSTR(xm->node->name), xm->node->line);
        return;
    }

    confirm_prop(xm->node, n, n, "name", errp);
    if (error_is_set(errp)) {
        return;
    }
            
    xm->last_node = xm->node;
    xm->node = first_node(xm->node->children);
}

static void xml_end_feature(Marshaller *m, Error **errp)
{
    XMLReader *xm = to_xm(m);

    if (!check_disabled(m)) {
        if (xm->node != NULL) {
            error_set(errp, "xml-reader", 0, "Extra nodes at end of `%s' on line %d",
                      "feature", xm->node->line);
            return;
        }

        xm->last_node = xm->last_node->parent;
        xm->node = next_node(xm->last_node);
    }
    xm->feature--;
}

static MarshallerOps xml_reader_ops = {
    .m_uint8 = xml_m_uint8,
    .m_uint16 = xml_m_uint16,
    .m_uint32 = xml_m_uint32,
    .m_uint64 = xml_m_uint64,
    .m_int8 = xml_m_int8,
    .m_int16 = xml_m_int16,
    .m_int32 = xml_m_int32,
    .m_int64 = xml_m_int64,
    .m_double = xml_m_double,
    .m_bool = xml_m_bool,
    .start_struct = xml_start_struct,
    .end_struct = xml_end_struct,
    .start_array = xml_start_array,
    .end_array = xml_end_array,
    .start_feature = xml_start_feature,
    .end_feature = xml_end_feature,
};

Marshaller *xml_reader_open(FILE *filep)
{
    XMLReader *xm;
    xmlNode *root;

    xm = malloc(sizeof(*xm));
    memset(xm, 0, sizeof(*xm));

    xm->m.ops = &xml_reader_ops;
    xm->features[xm->feature++] = true;

    xm->doc = xmlReadFd(fileno(filep), "anonymous.xml", NULL, 0);
    if (xm->doc == NULL) {
        return NULL;
    }

    root = xmlDocGetRootElement(xm->doc);

    xm->node = first_node(root->children);
    xm->last_node = xm->node;

    return &xm->m;
}

void xml_reader_close(Marshaller *m)
{
    XMLReader *xm = to_xm(m);

    xmlFreeDoc(xm->doc);
    xmlCleanupParser();
    free(xm);
}
