/*
 * QEMU Errors
 *
 * Copyright IBM, Corp. 2010
 *
 * Authors:
 *  Anthony Liguori <aliguori@us.ibm.com>
 *
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "error.h"

static void *mallocz(size_t size)
{
    void *ret;

    ret = malloc(size);
    if (ret) {
        memset(ret, 0, size);
    }
    return ret;
}

Error *error_new(const char *domain,
                 int code,
                 const char *format,
                 ...)
{
    Error *e;
    va_list ap;

    va_start(ap, format);
    e = error_new_valist(domain, code, format, ap);
    va_end(ap);

    return e;
}

Error *error_new_valist(const char *domain,
                        int code,
                        const char *format,
                        va_list ap)
{
    Error *e;

    e = mallocz(sizeof(*e));
    e->domain = strdup(domain);
    e->code = code;

    if (vasprintf(&e->message, format, ap) == -1) {
        abort();
    }

    return e;
}

Error *error_new_literal(const char *domain,
                         int code,
                         const char *message)
{
    Error *e;

    e = mallocz(sizeof(*e));
    e->domain = strdup(domain);
    e->code = code;
    e->message = strdup(message);

    return e;
}

void error_set(Error **errp,
               const char *domain,
               int code,
               const char *format,
               ...)
{
    if (errp) {
        va_list ap;

        va_start(ap, format);
        *errp = error_new_valist(domain, code, format, ap);
        va_end(ap);
    }
}

bool error_is_set(Error **errp)
{
    if (errp && *errp) {
        return true;
    }
    return false;
}

void error_propagate(Error **errp,
                     Error *err)
{
    if (errp) {
        *errp = err;
    } else {
        error_free(err);
    }
}

Error *error_copy(const Error *err)
{
    return error_new_literal(err->domain,
                             err->code,
                             err->message);
}

void error_free(Error *e)
{
    free(e->domain);
    free(e->message);
    free(e);
}

